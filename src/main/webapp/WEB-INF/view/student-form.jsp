<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>

<head>
    <title>Student Registration Form</title>
</head>

<body>


    <form:form action="processForm" modelAttribute="student">

        First name: <form:input path="firstName" />

        <br><br>

        Last name: <form:input path="lastName" />

        <br><br>


        <form:select path="country">
            <form:options items="${student.countryOptions}" />
        </form:select>

        <br><br>

        <form:select path="language">
            <form:options items="${student.languages}" />

        </form:select>


        <br><br>

        Favorite programming language:

        Java <form:radiobutton path="favoriteProgrammingLanguage" value="Java" />
        C# <form:radiobutton path="favoriteProgrammingLanguage" value="C#" />
        PHP <form:radiobutton path="favoriteProgrammingLanguage" value="PHP" />
        Ruby <form:radiobutton path="favoriteProgrammingLanguage" value="Ruby" />
        <br><br>

        Linux <form:checkbox path="operatingSystems" value="Linux" />
        Mac OS <form:checkbox path="operatingSystems" value="Mac OS" />
        MS Windows <form:checkbox path="operatingSystems" value="MS Windows" />

        <br><br>
        <input type="submit" value="Submit" />


    </form:form>


</body>

</html>

